output "gke_cluster_name" {
  value = google_container_cluster.default.name
}

output "cluster_service_account_email" {
  value = google_service_account.cluster_service_account.email
}

output "cluster_service_account_name" {
  value = google_service_account.cluster_service_account.name
}
